/**
 * Маршрутизация внутри контроллера
 */

'use strict';

angular.module('chickApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('about', {
        url: '/about',
        templateUrl: 'app/about/about.html',
        controller: 'AboutCtrl'
      });
  });
/**
 * Контроллер для работы с наборами данных
 */

'use strict';

angular.module('chickApp')
  .controller('DatasetCtrl', function ($scope, $http) {

  	// Переменные для работы в шаблоне
	$scope.dataset = {'lang': 'en', 'result_type': 'mixed' };
	$scope.datasets = [];

	// Функция для загрузки существующих наборов данных
	$scope.load = function () {
		// Делаем GET-запрос на сервер
		$http({ 
			method: 'GET', 
			url: '/api/datasets/' 
		}).then(function successCallback(response) { 
			// Передаем загруженные данные в шаблон
			$scope.datasets = response.data; 
		});
	}

	// Функция для создания нового набора данных
	$scope.create = function () {
		// Делаем POST-запрос на сервер
		$http({
			method: 'POST',
			url: '/api/datasets/',
			data: $scope.dataset
		}).then(function successCallback(response) {
			// Передаем загруженные данные в шаблон
			$scope.dataset = {'lang': 'en', 'result_type': 'mixed' };
			$scope.load();
		});
	}

	// Функция удаления набора данных
	$scope.delete = function (id) {
		// Делаем DELETE-запрос на сервер
		$http({
			method: 'DELETE',
			url: '/api/datasets/'+id
		}).then(function successCallback(response) {
			// Передаем загруженные данные в шаблон
			$scope.load();
		});
	}

	// Функция для загрузки данных в набор данных
	$scope.query = function (id, direction) {
		// Делаем PUT-запрос на сервер
		$http({
			method: 'PUT',
			url: '/api/datasets/'+id+'/'+direction
		}).then(function successCallback(response) {
			// Передаем загруженные данные в шаблон
			$scope.load();
		});
	}

	// После загрузки контроллера
	// загружаем наборы данных
	$scope.load();
  });

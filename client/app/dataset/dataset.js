/**
 * Маршрутизация внутри контроллера
 */

'use strict';

angular.module('chickApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('dataset', {
        url: '/dataset',
        templateUrl: 'app/dataset/dataset.html',
        controller: 'DatasetCtrl'
      });
  });
/**
 * Контроллер для просмотра набора данных
 */

'use strict';

angular.module('chickApp')
  .controller('MainCtrl', function ($scope, $http, $uibModal, moment) {
   
    // Переменные для работы в шаблоне
    $scope.dataset = {};
    $scope.datasets = [];
    $scope.twits = [];

    // Функция для просмотра подробной информации о твите
    $scope.details = function(status) {
      // Открываем модальное окно 
      var modalInstance = $uibModal.open({
        controller: 'ModalCtrl',
        templateUrl: 'app/main/main.modal.html',
        size: 'md',
        // Передаем в окно информацию о твите
        resolve: { status: status }
      });
    }

    // Функция загрузки списка наборов данных
    $scope.loadSets = function () {
      // Делаем GET-запрос на сервер
      $http({ 
        method: 'GET', 
        url: '/api/datasets/' 
      }).then(function successCallback(response) { 
        // Передаем данные в шаблон
        $scope.datasets = response.data; 
      });
    }

    // Функция загрузки определенного набора данных
    $scope.loadSet = function (id) {
      // Делаем GET-запрос на сервер
      $http({ 
        method: 'GET', 
        url: '/api/datasets/'+id 
      }).then(function successCallback(response) { 
        // Передаем данные в шаблон
        $scope.dataset = response.data; 
        // Загружаем твиты из набора данных
        $scope.loadTwits();
      });
    }

    // Функция загрузки твито из набора данных
    $scope.loadTwits = function () {
      // Делаем GET-запрос на сервер
      $http({ 
        method: 'GET', 
        url: '/api/twits/dataset/'+$scope.dataset.name
      }).then(function successCallback(response) { 
         // Передаем данные в шаблон
        $scope.twits = response.data; 
      });
    }

    // Функция построения графика
    $scope.buildGraph = function()
    {
      // Настройки графика 
      $scope.options = {
          chart: {
              type: 'multiBarChart',
              height: 450,
              margin : {
                  top: 20,
                  right: 20,
                  bottom: 45,
                  left: 45
              },
              clipEdge: true,
              staggerLabels: true,
              duration: 500,
              stacked: true,
              xAxis: {
                axisLabel: 'Дата',
                showMaxMin: false,
                tickFormat: function(d) {
                    return d3.time.format('%d.%m.%y')(new Date(Math.round(d)))
                },
              },
              yAxis: {
                axisLabel: 'Сообщения',
                axisLabelDistance: -20,
                tickFormat: function(d){
                    return d3.format(',f')(d);
                }
              }
          }
      };

      // Подготавливаем данные
      var data = {'neutral': [], 'negative':[], 'positive': []};
      var days = {};

      // Обходим все твиты 
      $scope.twits.forEach(function(element, id){
        // Определяем день в котором они были опубликнованы
        var day = moment(element.twit.created_at, "ddd MMM D HH:mm:ss +0000 YYYY").startOf('day').valueOf();
        if (days[day] == null) days[day] = [];
        
        // Добавляем твит в массив этого дня
        days[day].push(element.analyze.score);
      });

      // Проходим по всем дням из набора
      Object.keys(days).sort().forEach(function(day){

          // Считаем количество позитивных, негативных и нейтреальных твитов
          var positive = days[day].filter( function(value) { return value >= 2; }).length;
          var negative = days[day].filter( function(value) { return value <= -2; }).length;
          var neutral = days[day].length - negative - positive;

          // Добавляем эту информацию в массив данных для графика
          data['neutral'].push({ x: day, y: neutral});
          data['negative'].push({ x: day, y: negative});
          data['positive'].push({ x: day, y: positive});
      });

      // Передаем данные в шаблон для построения графика
      $scope.data = [
        {key: 'Нейтральный', values:data['neutral'], 'color': '#3182bd'}, 
        {key: 'Негативный', values: data['negative'], 'color': '#e6550d'}, 
        {key: 'Позитивный', values: data['positive'], 'color': '#31a354' } ];
    }

    // Настройки для облака
    var stopwords = ["a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren't", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't", "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's", "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours  ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves"];
    var minSize = 5;
    var maxSize = 255;

    // Функция построения облака
    $scope.buildCloud = function()
    {
      // Настройки облака
      $scope.cloud = {};

      // Обходим все твиты
      $scope.twits.forEach(function(element, id){
        
        // Получаем текст твита в нижнем регистре и без переносов
        var text = element.twit.text.toLowerCase().replace('\n', " ").replace(',', " ");
        
        // Делим текст на слова
        var words = text.split(' ');
        
        // Обходим все слова
        words.forEach(function(element){ 
          // Игнорируем ссылки
          if (element.substr(0,4) == 'http') return;
          
          // Игнорируем стоп-слова
          if (stopwords.indexOf(element) != -1) return;
          
          //  Добавляем в массив со счетчиком слов
          $scope.cloud[element] = $scope.cloud[element] == null ? 1 : $scope.cloud[element] + 1; 
          if ($scope.cloud[element] > maxSize) $scope.cloud[element] = maxSize;
        });
      });

      // Подготавливаем данные для передачи в облако
      var list = [];
      Object.keys($scope.cloud).forEach(function(key){ list.push([key, $scope.cloud[key]]);});

      // Передаем данные для генерации облака
      WordCloud(document.getElementById('canvas'), { list: list, minSize: minSize } );
    }

    // После загрузки контроллера загружаем информацию о существующих наборах
    $scope.loadSets();
  })
  // Дополнительные контроллер для модального окна
  .controller('ModalCtrl', function ($scope, $uibModalInstance, status) {

    // Получаем информацию о твите для просмотра
    $scope.status = status;

    // Закрываем окно 
    $scope.ok = function () {
      $uibModalInstance.close();
    };

    // Отказываемся от изменений и закрываем окно
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });

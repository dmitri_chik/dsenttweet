/**
 * Маршрутизация внутри контроллера
 */

'use strict';

angular.module('chickApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });
  });
/**
 * Контроллер для просмотра 
 * результатов запроса к твиттеру
 */

'use strict';

angular.module('chickApp')
  .controller('RequestCtrl', function ($scope, $uibModal, $http) {
    // Переменные для работы в шаблоне
    $scope.search = {'text': '', 'since': '2016-12-01', 'count': 1000 }

    // Функция для получения данных из твиттера
    $scope.searchIt = function() {
      $http.post('/api/things', { text: $scope.search.text, since: $scope.search.since, count: $scope.search.count  })
        .success(function(results) {
          $scope.results = results;
        });
    };
    
    // Функция для просмотра подробной информации о твите
    $scope.details = function(index){
      // Открываем модальное окно 
      var modalInstance = $uibModal.open({
        controller: 'RequestModalCtrl',
        templateUrl: 'app/request/request.modal.html',
        size: 'md',
        resolve: {
          // Передаем в окно информацию о твите
          status: $scope.results[index]
        }
      });
    }
  })
  // Дополнительные контроллер для модального окна 
  .controller('RequestModalCtrl', function ($scope, $uibModalInstance, status) {

    // Получаем информацию о твите для просмотра
    $scope.status = status;

    // Закрываем окно 
    $scope.ok = function () {
      $uibModalInstance.close();
    };

    // Отказываемся от изменений и закрываем окно
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });


'use strict';

angular.module('chickApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('request', {
        url: '/request',
        templateUrl: 'app/request/request.html',
        controller: 'RequestCtrl'
      });
  });
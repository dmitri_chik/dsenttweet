/**
 * Контроллер для работы с меню приложения
 */

'use strict';

angular.module('chickApp')
  .controller('NavbarCtrl', function ($scope, $location) {

    // Массив элементов для меню сайта
    $scope.menu = [{
      'title': 'Главная',
      'link': '/'
    },
    {
      'title': 'Запрос',
      'link': '/request'
    },
    {
      'title': 'Данные',
      'link': '/dataset'
    },
    {
      'title': 'О программе',
      'link': '/about'
    }];

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
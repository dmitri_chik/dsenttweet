var Twit = require('twit');

var analyze = require('Sentimental').analyze,
    positivity = require('Sentimental').positivity,
    negativity = require('Sentimental').negativity;

var MongoClient = require('mongodb').MongoClient, 
    Logger = require('mongodb').Logger,
    assert = require('assert');

// Set debug level
//Logger.setLevel('info');
// Connection URL
var url = 'mongodb://localhost:27017/chick-dev';

var theme = process.argv[2] || 'putin';
var timeline = process.argv[3] || 'older';
var times = process.argv[4] || 1;

var T = new Twit({
  consumer_key:         'DALHJ9saTtzFEklKwCKDiHtRi',
  consumer_secret:      'sPUSbz5j73JsMsN5mdJGM2eRUR1OsWwJvYnz5bhFUfayIM3vgM',
  app_only_auth:        true,
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});

MongoClient.connect(url, function(err, db) {

	var collection = db.collection(theme);

	// Если нужны более свежие твиты
	if (timeline == 'newer')
	{
		console.log('Searching for newer twits!');

		collection.find().sort({id:-1}).limit(1).toArray(function(err, docs) {
		    console.log("Found the following records:\n");
		   	if (docs.length == 0) since = 0;
		   	else since = docs[0].id;
		   	console.log("Using since_id:"+since);

			T.get('search/tweets', { q: theme, since_id:since, lang: 'en', count: 100 }, function(err, data, response) {
			  data.statuses.forEach(function(status, index) {
			  	status['analyze'] = analyze(status.text);
				collection.insertMany([ status ], 
					function(err, result) {
						console.log(err);
						console.log("Inserted document ("+status.created_at+"): "+status.text);
					});
				});

			  db.close();
			});
		});
	}
	else
	{
		console.log('Searching for older twits!');

		collection.find().sort({id:1}).limit(1).toArray(function(err, docs) {
		    console.log("Found the following records:\n");
		   	if (docs.length == 0) max_id = 0;
		   	else max_id = docs[0].id;
		   	console.log("Using max_id:"+max_id);

			T.get('search/tweets', { q: theme, max_id:max_id, lang: 'en', count: 100 }, function(err, data, response) {
			  data.statuses.forEach(function(status, index) {
			  	status['analyze'] = analyze(status.text);
				collection.insertMany([ status ], 
					function(err, result) {
						console.log(err);
						console.log("Inserted document ("+status.created_at+"): "+status.text);
					});
				});

			  db.close();
			});
		});
	}

});
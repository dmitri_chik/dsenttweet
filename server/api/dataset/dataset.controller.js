/**
 * Контроллер для работы с наборами данных
 */

'use strict';

var _ = require('lodash');
var Dataset = require('./dataset.model');
var Twit = require('../twit/twit.model');

var Twitter = require('twit');
var T = new Twitter({
  consumer_key: 'DALHJ9saTtzFEklKwCKDiHtRi',
  consumer_secret: 'sPUSbz5j73JsMsN5mdJGM2eRUR1OsWwJvYnz5bhFUfayIM3vgM',
  app_only_auth: true,
  timeout_ms: 60*1000,  
});

var analyze = require('Sentimental').analyze,
    positivity = require('Sentimental').positivity,
    negativity = require('Sentimental').negativity;

// Просмотр всех наборов данных
exports.index = function(req, res) {
  Dataset.find(function (err, datasets) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(datasets);
  });
};

// Информация о выбранном наборе данных
exports.show = function(req, res) {
  Dataset.findById(req.params.id, function (err, dataset) {
    if(err) { return handleError(res, err); }
    if(!dataset) { return res.status(404).send('Not Found'); }
    return res.json(dataset);
  });
};

// Создание нового набора данных
exports.create = function(req, res) {
  Dataset.create(req.body, function(err, dataset) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(dataset);
  });
};

// Загрузка твитов в набор данных
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }

  Dataset.findById(req.params.id, function (err, dataset) {
    if (err) { return handleError(res, err); }
    if(!dataset) { return res.status(404).send('Not Found'); }

    if (req.params.direction == 'newer')
    {
      console.log('Searching for newer twits!', dataset);

      if (dataset.last == null) dataset.last = {'id': 0 };

      T.get('search/tweets', { q: dataset.query, since_id:dataset.last.id, lang: dataset.lang, result_type: dataset.result_type, count: 100 }, function(err, data, response) {
        
        var last = dataset.last;
        data.statuses.forEach(function(status, index) {
          if (status.text.substr(0,2) == 'RT') return;
          Twit.create({'dataset': dataset.name, 'analyze': analyze(status.text), twit: status }, function(err, twit) {
            if(err) { return handleError(res, err); }
          });

          if (status.id > last.id) { last = { 'id': status.id, 'date': status.created_at } };
          dataset.count++;
        });

        dataset.last = last;
        dataset.save(function (err) {
          if (err) { return handleError(res, err); }
          return res.status(200).json(dataset);
        });
      });
    }
    else
    {
      console.log('Searching for older twits!', dataset);

      if (dataset.first == null) dataset.first = {'id': 0 };

      T.get('search/tweets', { q: dataset.query, max_id:dataset.first.id, lang: dataset.lang, result_type: dataset.result_type, count: 100 }, function(err, data, response) {
        
        var first = dataset.first;
        data.statuses.forEach(function(status, index) {
          if (status.text.substr(0,2) == 'RT') return;
          Twit.create({'dataset': dataset.name, 'analyze': analyze(status.text), twit: status }, function(err, twit) {
            if(err) { return handleError(res, err); }
          });

          if (status.id < first.id || first.id == 0) { first = { 'id': status.id, 'date': status.created_at } };
          dataset.count++;
        });

        dataset.first = first;
        dataset.save(function (err) {
          if (err) { return handleError(res, err); }
          return res.status(200).json(dataset);
        });
      });
    }
  });
};

// Удаление набора данных
exports.destroy = function(req, res) {
  Dataset.findById(req.params.id, function (err, dataset) {
    if(err) { return handleError(res, err); }
    if(!dataset) { return res.status(404).send('Not Found'); }
    dataset.remove(function(err) {
      if(err) { return handleError(res, err); }

      Twit.find({dataset: dataset.name}).remove(function(err) {
        if(err) { return handleError(res, err); }
        return res.status(204).send('No Content');
      });
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
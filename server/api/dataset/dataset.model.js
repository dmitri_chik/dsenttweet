/**
 * Схема для хранения 
 * набора данных в БД
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DatasetSchema = new Schema({
  name: String,
  query: String,
  lang: String,
  result_type: String,
  count: { type: Number, default: 0 },
  first: {},
  last: {}
});

module.exports = mongoose.model('Dataset', DatasetSchema);
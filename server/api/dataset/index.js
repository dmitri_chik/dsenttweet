/**
 * Маршрутизация запросов к контроллеру 
 */

'use strict';

var express = require('express');
var controller = require('./dataset.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/:direction', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
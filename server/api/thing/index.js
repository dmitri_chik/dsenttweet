/**
 * Маршрутизация запросов к контроллеру 
 */

'use strict';

var express = require('express');
var controller = require('./thing.controller');

var router = express.Router();

router.post('/', controller.query);

module.exports = router;
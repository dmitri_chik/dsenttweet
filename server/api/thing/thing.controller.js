/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Twit = require('twit');
var analyze = require('Sentimental').analyze,
    positivity = require('Sentimental').positivity,
    negativity = require('Sentimental').negativity;

var T = new Twit({
  consumer_key:         'DALHJ9saTtzFEklKwCKDiHtRi',
  consumer_secret:      'sPUSbz5j73JsMsN5mdJGM2eRUR1OsWwJvYnz5bhFUfayIM3vgM',
  app_only_auth:        true,
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});

// Creates a new thing in the DB.
exports.query = function(req, res) {

  var results = {'statuses': [] };

  T.get('search/tweets', { q: req.body.text+' since:'+req.body.since, lang: 'en', count: req.body.count }, function(err, data, response) {
    data.statuses.forEach(function(status, index) {
      console.log(status);
      results.statuses.push({
          'user': {
            'screen_name': status.user.screen_name,
            'name': status.user.name,
            'avatar': status.user.profile_image_url
          },
          'status': {
            'text': status.text,
            'created_at': status.created_at,
            'analyze': analyze(status.text)
          }
        });
    });

    return res.status(200).json(results);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
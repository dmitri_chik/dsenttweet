'use strict';

var _ = require('lodash');
var Twit = require('./twit.model');

// Get list of twits
exports.index = function(req, res) {
  Twit.find(function (err, twits) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(twits);
  });
};

// Get a single twit
exports.show = function(req, res) {
  Twit.findById(req.params.id, function (err, twit) {
    if(err) { return handleError(res, err); }
    if(!twit) { return res.status(404).send('Not Found'); }
    return res.json(twit);
  });
};


// Get a single twit
exports.showdataset = function(req, res) {
  Twit.find({dataset: req.params.id}, function (err, twit) {
    if(err) { return handleError(res, err); }
    if(!twit) { return res.status(404).send('Not Found'); }
    return res.json(twit);
  });
};

// Creates a new twit in the DB.
exports.create = function(req, res) {
  Twit.create(req.body, function(err, twit) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(twit);
  });
};

// Updates an existing twit in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Twit.findById(req.params.id, function (err, twit) {
    if (err) { return handleError(res, err); }
    if(!twit) { return res.status(404).send('Not Found'); }
    var updated = _.merge(twit, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(twit);
    });
  });
};

// Deletes a twit from the DB.
exports.destroy = function(req, res) {
  Twit.findById(req.params.id, function (err, twit) {
    if(err) { return handleError(res, err); }
    if(!twit) { return res.status(404).send('Not Found'); }
    twit.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
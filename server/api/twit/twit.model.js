'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TwitSchema = new Schema({
  dataset: String,
  analyze: {},
  twit: {}
});

module.exports = mongoose.model('Twit', TwitSchema);